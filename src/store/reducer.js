import { combineReducers } from "redux";
import auth from "./reducers/auth";
import restaurant from "./reducers/restaurant";
import user from "./reducers/user";

let reducer;
export default reducer = combineReducers(
    {
        auth: auth,
        restaurant: restaurant,
        user: user,
    },
);
