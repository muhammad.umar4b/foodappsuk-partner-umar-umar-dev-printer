import SoundPlayer from "react-native-sound-player";

export const Slugify = (str) => {
    str = str.replace(/^\s+|\s+$/g, "");

    // Make the string lowercase
    str = str.toLowerCase();

    // Remove accents, swap ñ for n, etc
    const from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
    const to = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
    for (let i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
    }

    // Remove invalid chars
    str = str.replace(/[^a-z0-9 -]/g, "")
        // Collapse whitespace and replace by _
        .replace(/\s+/g, "_")
        // Collapse dashes
        .replace(/-+/g, "_");

    return str;
};

export function getTotalPrice(foodItems) {
    console.log("fooditems", foodItems);
    let totalAmount = 0;

    foodItems.forEach(item => {
        const qty = parseInt(item.quantity);
        const price = parseFloat(item.price).toFixed(2);
        totalAmount += qty * price;
    });

    return totalAmount;
}

export function playSound() {
    try {
        SoundPlayer.playSoundFile("nuclearalarm", "mp3");
    } catch (e) {
        console.log(`cannot play the sound file`, e);
    }
}
