import React  from "react";
import { Text, View } from "react-native";

import moment from "moment";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

export default function Header({ orderDetails, restaurantInfo }) {
    const {
        restaurantHeader,
    } = styles;

    const {
        f18,
        textCenter,
        fw700,
    } = globalStyles;

    const {
        createdAt,
        orderNumber,
        orderStatus,
        orderType
    } = orderDetails;

    const {
        name,
        address,
    } = restaurantInfo;

    return (
        <View style={restaurantHeader}>
            <Text style={[f18, textCenter, fw700]}>
                {name}
            </Text>

            <Text style={textCenter}>
                {address}
            </Text>

            <Text style={textCenter}>
                {orderType} ({moment(createdAt).format("DD/MM/YYYY")})
            </Text>

            <Text style={textCenter}>
                {orderNumber} ({moment(createdAt).format("hh:mm A")})
            </Text>
        </View>
    );
};
