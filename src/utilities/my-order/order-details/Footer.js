import React from "react";
import { Linking, Text, TouchableOpacity, View } from "react-native";

import Ionicons from "react-native-vector-icons/Ionicons";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

export default function Footer({ orderDetails }) {
    const {
        deliveryArea,
    } = styles;

    const {
        textCenter,
        f18,
        fw700,
        flexDirectionRow,
        justifyCenter,
        paddingTop3,
    } = globalStyles;

    const {
        orderType,
        mobileNo,
        customerName,
        deliveredAt,
        tableNo,
        postCode
    } = orderDetails;

    const cartItem = JSON.parse(orderDetails["cartItems"]);
    const { note } = cartItem;

    return (
        <>
            <View style={deliveryArea}>
                <Text style={[fw700, textCenter, f18]}>
                    Kitchen Notes: {note}</Text>
            </View>

            {orderType === "Home Delivery" &&
                <View style={deliveryArea}>
                    <Text style={[fw700, textCenter, f18]}>
                        Delivered To: {customerName}
                    </Text>
                    <Text style={[fw700, textCenter, f18]}>
                        {deliveredAt}{postCode ? (", " + postCode) : ""}
                    </Text>
                </View>
            }

            {orderType === "Collection" &&
                <View style={deliveryArea}>
                    <Text style={[fw700, textCenter, f18]}>
                        Collection For: {customerName}
                    </Text>
                </View>
            }

            {["Dine In", "Counter"].includes(orderType) &&
                <View style={deliveryArea}>
                    <Text style={[fw700, textCenter, f18]}>
                        Table No: {tableNo}
                    </Text>
                </View>
            }

            <Text style={[fw700, textCenter, f18]}>
                Guest No: {mobileNo ? mobileNo : "N/A"}
            </Text>

            {mobileNo ?
                <View style={[flexDirectionRow, justifyCenter, paddingTop3]}>
                    <TouchableOpacity onPress={() => Linking.openURL(`tel:${mobileNo}`)}>
                        <Ionicons name="call" size={32} color="green" />
                    </TouchableOpacity>
                </View>
                :
                <Text />
            }
        </>
    );
}
