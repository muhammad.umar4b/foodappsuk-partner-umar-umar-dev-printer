import React, {useEffect, useLayoutEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import axios from 'axios';
import {useSelector} from 'react-redux';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

import {apiBaseUrl} from '../../../config/index-example';
import {printContent} from '../../order-print';

// Component
import Body from './Body';
import Footer from './Footer';
import Header from './Header';
import PriceArea from './PriceArea';
import Loader from '../../Loader';

const OrderDetails = ({route, navigation}) => {
  const {id} = route.params;
  const tableHead = ['Qty', 'Description', 'Amount'];
  const [tableData, setTableData] = useState([]);
  const [orderDetails, setOrderDetails] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isPrintButtonDisable, setIsPrintButtonDisable] = useState(false);

  const state = useSelector(state => state);
  const {
    restaurant: {restaurantData},
  } = state;

  const getOrderDetails = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}order/get-single-order/${id}`,
      );
      if (response.data) {
        setOrderDetails(response.data.data);
        setIsLoading(false);
        return true;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  useEffect(() => {
    getOrderDetails(id).then(res => console.log('ORDER DETAILS: ', res));
  }, [id]);

  useEffect(() => {
    if (orderDetails) {
      const cartItem = JSON.parse(orderDetails['cartItems']);
      if (cartItem) {
        let updateTableData = [];
        cartItem.foodItems.forEach(item => {
          const name =
            item.options.length > 0
              ? `${item.name} (${item.options.join(', ')})`
              : item.name;
          const arr = [item.quantity, name, parseFloat(item.price).toFixed(2)];
          updateTableData.push(arr);
        });

        setTableData(updateTableData);
      }
    }
  }, [orderDetails]);

  const {container, button, buttonText} = styles;

  const {marginRight4, marginLeft2, flexDirectionRow} = globalStyles;

  useLayoutEffect(() => {
    if (orderDetails && restaurantData) {
      const {orderType, orderStatus} = orderDetails;

      navigation.setOptions({
        headerRight: () => (
          <View style={flexDirectionRow}>
            {orderType === 'Dine In' && orderStatus === 'Pending' && (
              <TouchableOpacity
                style={[button, marginLeft2]}
                onPress={() =>
                  navigation.navigate('InHouseDineInPayment', {
                    orderDetailsId: id,
                  })
                }>
                <Text style={buttonText}>Serve</Text>
              </TouchableOpacity>
            )}
            <View style={marginRight4}>
              <TouchableOpacity
                style={[button, marginLeft2]}
                onPress={() => {
                  setIsPrintButtonDisable(true);
                  setTimeout(() => {
                    setIsPrintButtonDisable(false);
                  }, 2000);
                  printContent(orderDetails, restaurantData);
                }}
                disabled={isPrintButtonDisable}>
                <Text style={buttonText}>Print</Text>
              </TouchableOpacity>
            </View>
          </View>
        ),
      });
    }
  }, [navigation, orderDetails, restaurantData, isPrintButtonDisable]);

  return isLoading ? (
    <Loader />
  ) : (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={container}>
          <Header orderDetails={orderDetails} restaurantInfo={restaurantData} />
          <Body tableHeadData={tableHead} tableData={tableData} />
          <PriceArea orderDetails={orderDetails} />
          <Footer orderDetails={orderDetails} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default OrderDetails;
