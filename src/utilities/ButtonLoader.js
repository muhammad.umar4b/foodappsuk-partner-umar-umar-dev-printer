import React from "react";
import { ActivityIndicator } from "react-native";

const ButtonLoader = ({ size = "small", color = "white" }) => {
    return (
        <ActivityIndicator size={size} color={color} />
    );
};


export default ButtonLoader;
