import React from "react";
import {Text} from "react-native";

import globalStyles from "../../assets/styles/globalStyles";
import SingleItem from "./SingleItem";

const RenderData = (props) => {
    const {
        data,
        isInHouse,
        navigation
    } = props;

    const {
        bgRed,
        bgSuccess,
        paddingTop1,
    } = globalStyles;

    const setStatusBgColor = (orderStatus) => {
        if (orderStatus === "Accepted") return bgSuccess;
        else if (orderStatus === "Canceled") return bgRed;
    };

    return data.length > 0 ?
        data.map((item, index) => <SingleItem
            navigation={navigation}
            item={item}
            key={index}
            setStatusBgColor={setStatusBgColor}
            isInHouse={isInHouse}
        />)
        :
        <Text style={paddingTop1}>No Order Yet!</Text>;
};


export default RenderData;
