import Home from "../screens/home/Home";

import Login from "../screens/auth/Login";
import Loading from "../screens/auth/Loading";
import MyBookings from "../screens/my-booking/MyBookings";
import MyOrder from "../screens/my-order/MyOrder";
import BusinessProfile from "../screens/business-profile/BusinessProfile";
import Printer from "../screens/printer/PrinterScreen";
import SalesReport from "../screens/sales-report/SalesReport";
import HouseOrder from "../screens/home/HouseOrder";
import Order from "../screens/home/Order";
import Register from "../screens/auth/Register";
import ForgotPassword from "../screens/auth/ForgotPassword";
import ResetPassword from "../screens/auth/ResetPassword";
import Chat from "../screens/home/Chat";
import Settings from "../screens/home/Settings";
import Driver from "../screens/home/Driver";
import Notifications from "../screens/notification/Notifications";

export {
    Home,
    Login,
    Loading,
    Register,
    Driver,
    SalesReport,
    MyBookings,
    BusinessProfile,
    HouseOrder,
    Printer,
    Order,
    MyOrder,
    ForgotPassword,
    ResetPassword,
    Chat,
    Settings,
    Notifications,
};
