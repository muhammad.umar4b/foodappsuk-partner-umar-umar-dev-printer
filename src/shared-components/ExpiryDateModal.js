import React from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";

import Modal from "react-native-modal";

import globalStyles from "../assets/styles/globalStyles";

export default function ExpiryDateModal(props) {
    const {
        isModalVisible,
        setModalVisible,
        monthList,
        state,
        setState,
        month,
        year,
        yearList,
    } = props;

    const {
        expiryDateModalView,
        expiryDateModalHeaderText,
        expiryDateModalHeader,
        expiryDateModalBody,
        expiryDateModalFooter,
        monthArea,
        monthAreaHeaderText,
        monthAreaContent,
        monthAreaContentBlock,
        monthAreaContentText,
        monthAreaContentTextActive,
        closeButton,
        continueText,
    } = globalStyles;

    return (
        <Modal
            isVisible={isModalVisible}
            style={expiryDateModalView}
        >
            <View>
                <View style={expiryDateModalHeader}>
                    <Text style={expiryDateModalHeaderText}>
                        Expiry Month/Year
                    </Text>
                </View>
                <View style={expiryDateModalBody}>
                    <View style={monthArea}>
                        <Text style={monthAreaHeaderText}>Month</Text>
                        <ScrollView style={monthAreaContent}>
                            {monthList.map((item, index) => (
                                <TouchableOpacity
                                    style={monthAreaContentBlock}
                                    key={index}
                                    onPress={() => setState({ ...state, month: item })}
                                >
                                    <Text style={[monthAreaContentText, month === item && monthAreaContentTextActive]}>
                                        {item}
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    </View>

                    <View style={monthArea}>
                        <Text style={monthAreaHeaderText}>Year</Text>
                        <ScrollView style={monthAreaContent}>
                            {yearList.map((item, index) => (
                                <TouchableOpacity
                                    style={monthAreaContentBlock}
                                    key={index}
                                    onPress={() => setState({ ...state, year: item })}
                                >
                                    <Text style={[monthAreaContentText, year === item && monthAreaContentTextActive]}>
                                        {item}
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    </View>
                </View>

                <View style={expiryDateModalFooter}>
                    <TouchableOpacity
                        style={closeButton}
                        onPress={() => setModalVisible(!isModalVisible)}
                    >
                        <Text style={continueText}>Ok</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
}
