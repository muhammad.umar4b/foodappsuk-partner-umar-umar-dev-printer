import React, { useEffect, useState } from "react";

import { useSelector } from "react-redux";

import NotificationBody from "./NotificationBody";
import NotFoundMessage from "./NotFoundMessage";

import { printUserContent } from "../../utilities/order-print";
import { acceptOrder, declineOrder } from "../../store/actions/order";
import { showToastWithGravityAndOffset } from "../../shared-components/ToastMessage";

const OrderNotification = ({ navigation, route }) => {
    const { title, message, data } = route.params;
    const state = useSelector(state => state);
    const [orderDetails, setOrderDetails] = useState(null);
    const { restaurant: { restaurantData } } = state;

    useEffect(() => {
        if (data.orderId) setOrderDetails(JSON.parse(data.orderId));
    }, [data]);

    const acceptOrderItem = () => {
        acceptOrder(orderDetails._id).then(res => {
            console.log("INHOUSE ORDER ACCEPT : ", !!res);
            if (res) {
                showToastWithGravityAndOffset("Order accepted successfully!");
                printUserContent(orderDetails, restaurantData);
                navigation.navigate("MyOrder", { pending: false });
            }
        });
    };

    const declineOrderItem = () => {
        declineOrder(orderDetails._id).then(res => {
            console.log("INHOUSE ORDER DECLINE : ", !!res);
            if (res) {
                showToastWithGravityAndOffset("Order declined successfully!");
                navigation.navigate("MyOrder", { pending: false });
            }
        });
    };

    return (
        route.params.data ?
            <NotificationBody
                acceptOrder={acceptOrderItem}
                declineOrder={declineOrderItem}
                title={title}
                message={message}
            />
            :
            <NotFoundMessage />
    );
};

export default OrderNotification;
