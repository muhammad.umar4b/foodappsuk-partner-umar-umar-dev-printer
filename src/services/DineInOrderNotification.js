import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { useSelector } from "react-redux";
import { printDineInContent } from "../utilities/order-print";

const DineInOrderNotification = ({ navigation, route }) => {
    const { data, title, message } = route.params;
    const [orderInfo, setOrderInfo] = useState(null);
    const { restaurant: { restaurantInfo } } = useSelector(state => state);

    useEffect(() => {
        if (Object.keys(data).length > 0 && data["data_string"]) {
            setOrderInfo(JSON.parse(data["data_string"]));
        } else {
            navigation.navigate("Home");
        }
    }, [route.params]);

    const acceptOrder = async () => {
        printDineInContent(orderInfo, restaurantInfo);

        if (orderInfo.orderFrom === "owner") {
            navigation.navigate("InHouseOrder", { order: false, dineIn: true });
        } else {
            navigation.navigate("MyOrder", { activeTab: 2 });
        }
    };

    return (
        orderInfo ?
            <View style={styles.container}>
                <View>
                    <Text style={styles.restaurantName}>{title ? title : "N/A!"}</Text>
                    <Text>{message ? message : "N/A"}</Text>
                </View>
                <View>
                    <View style={styles.acceptButtonArea}>
                        <TouchableOpacity style={[styles.acceptButton]} onPress={acceptOrder}>
                            <Text style={styles.acceptText}>Accept</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            :
            <View style={styles.notFoundArea}>
                <Text>Order Information Not found!</Text>
            </View>

    );
};

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: "#fff",
            elevation: 3,
            marginHorizontal: wp("5%"),
            marginVertical: hp("2%"),
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("2%"),
            borderRadius: 10,
        },
        notFoundArea: {
            backgroundColor: "#fff",
            elevation: 3,
            marginHorizontal: wp("5%"),
            marginVertical: hp("2%"),
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("2%"),
            borderRadius: 10,
        },
        restaurantName: {
            fontSize: 18,
        },
        acceptButtonArea: {
            marginTop: hp("2%"),
        },
        acceptButton: {
            backgroundColor: "#1fbf21",
            borderRadius: 8,
            height: hp("41%"),
            justifyContent: "center",
            alignItems: "center",
        },
        acceptText: {
            color: "#fff",
            fontSize: 30,
            fontWeight: "700",
        },
    },
);

export default DineInOrderNotification;
