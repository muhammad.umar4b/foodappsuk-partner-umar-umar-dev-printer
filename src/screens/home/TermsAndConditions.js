import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import axios from 'axios';
import {apiBaseUrl} from '../../config/index-example';
import HTML from 'react-native-render-html';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import globalStyles from '../../assets/styles/globalStyles';

// const Loader = () => {
//     return (
//         <View
//             style={styles.loadingArea}>
//             <ActivityIndicator size="large" color="#D2181B" />
//         </View>
//     );
// };

const TermsAndConditions = () => {
  // const [data, setData] = useState(null);
  // const [isLoading, setIsLoading] = useState(true);

  // const getTermsAndConditions = async ownerId => {
  //     try {
  //         const response = await axios.get(`${apiBaseUrl}pcf/fetch-terms-owner`);
  //         if (response.data && response.data.length > 0) {
  //             setData(response.data[0]);
  //             setIsLoading(false);
  //             return true;
  //         }
  //     } catch (error) {
  //         if (error.response.data) {
  //             console.log(error.response.data);
  //         }
  //     }
  // };

  // useEffect(() => {
  //     getTermsAndConditions().then(res => console.log("TERMS & CONDITIONS: ", res));
  // }, []);

  // const renderItem = data ?
  //     <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
  //         <HTML source={{ html: data.description }} />
  //     </ScrollView>
  //     :
  //     <View style={styles.container}>
  //         <Text style={globalStyles.paddingTop2}> No terms of use available!</Text>
  //     </View>;

  return (
    <View style={{flex: 1}}>
      <ScrollView
        contentContainerStyle={{}}
        showsVerticalScrollIndicator={false}>
        <View style={{margin: '4%', paddingBottom: heightPercentageToDP(6)}}>
          {/* first phragraph */}
          <View style={{margin: '4%'}}>
            <Text style={{color: 'blue', fontSize: 20, fontWeight: 'bold'}}>
              Business Owner Policy
            </Text>
            <Text style={{color: 'black'}}>
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>FoodApps UK </Text>{' '}
              <Text style={{fontWeight: 'bold'}}>
                tells you the terms and conditions on which food and drink will
                be collected{' '}
              </Text>
              from the listed Restaurants or Takeaways and deliver to the
              allocated customers.
              {'\n'}
              {'\n'}
              Please read these terms and conditions carefully before complete
              the registration as
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>FoodApps Partner </Text> by
              mark the read option you should understand that you are agree to
              be bound by these terms and conditions. If you refuse to accept
              these terms and conditions, you will not be able complete the
              registration through this website.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>www.foodapps.uk </Text>{' '}
              operated by NETSOFTUK SOLUTION LTD which is trading name of this
              Company. We are registered in UK under company NO-12235478 and
              with our registered office- Unit-3, 2a Beaconsfield Street,
              Newcastle Upon Tyne, Tyne & Wear, United Kingdom,
              {'\n'}
              NE4 5JN
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>FoodApps </Text> is only
              intended for{' '}
              <Text style={{fontWeight: 'bold'}}>Business Owner </Text>by
              people’s post code resident in their specified Ares by this
              website. The purpose of this website is able to deliver the Foods
              Food from the Chosen Restaurants or Takeaways.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>Cancel Policy: </Text>
              {'\n'}
              {'\n'}
              Once an order is placed, you will be provided the option to cancel
              the order up until the point of acceptance from the Outlet. You
              will not have any right to cancel an order once accepted by the
              Outlet.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>Delivery Policy: </Text>
              {'\n'}
              {'\n'}
              The Outlet ́s goal is to deliver or make available on-time i.e.
              delivering at the time it quotes. Unfortunately factors, such as,
              weather and traffic conditions occasionally prevent the Outlet
              from achieving this. The Outlet cannot be responsible for late
              delivery.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>Right to Business: </Text>It’s
              <Text style={{fontWeight: 'bold'}}>Business Owner </Text>{' '}
              responsibility to comply all legal requirements with local council
              as well as health & safety policy to run the Food Business. We are
              not liable any responsibility if they are fail to maintain any
              points of legal points.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>Not show policy: </Text> All
              drivers need to inform Restaurants/ Takeaways if any Customer No
              Show their visibility to pick up their Food. They have to wait at
              list 10 Minutes to confirm NO SHOW massage to customer Support and
              return the Food to Restaurants/ Takeaways.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>Terminate by the </Text>{' '}
              Restaurants/ Takeaways: Restaurants/ Takeaways can terminate the
              Customer’s order due to payment fail or anti-social behaviour by
              the Customers.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>Payment Policy: </Text>
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>7.1 </Text> Payment for all{' '}
              <Text style={{fontWeight: 'bold'}}>Business Owner </Text> will be
              weekly payment via direct pay to their own Bank. Other payment
              option needs to inform to customer support team. Bank charges are
              2% of price plus 20 pens per order. from the Chosen Restaurants or
              Takeaways.
              {'\n'}
              {'\n'}
              <Text style={{fontWeight: 'bold'}}>Private Policy: </Text>
              {'\n'}
              We are committed to safeguarding the privacy of our website
              visitors; this policy sets out how we will treat your personal
              information. Our website uses cookies. By using our website and
              agreeing to this policy, you consent to our use of cookies in
              accordance with the terms of this policy.
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>

    // isLoading ? <Loader /> : renderItem
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: wp('5%'),
  },
  loadingArea: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
});

export default TermsAndConditions;
