import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView, Linking, ToastAndroid } from "react-native";
import { connect } from "react-redux";
import { BLEPrinter } from "react-native-thermal-receipt-printer";
import BluetoothStateManager from "react-native-bluetooth-state-manager";
import { Header, Container, Left, Right, Body, Icon, Title, Button } from "native-base";
import Timeline from "react-native-timeline-flatlist";
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from "react-native-table-component";
import moment from "moment";
import axios from "axios";

export class Order extends Component {
    state = {
        resturentName: "",
        resturentLocation: "",
        orderNumber: "",
        name: "",
        userAddress: "",
        userPhone: "",
        orderDate: "",
        totalPrice: "",
        printing: true,
        printers: [],
        currentPrinter: "",
        foodItems: [],
        paymentMethod: "",
        deliveryCharge: "",
        discountCharge: "",
        discount: "",
        data: [{ time: "0", title: "Received", lineColor: "#d2181b", circleColor: "#d2181b" },
            { time: "0", title: "Picked up", lineColor: "#adadad", circleColor: "#d2181b" },
            { time: "0", title: "Delivered", lineColor: "#adadad", circleColor: "#adadad" }],
        tableHead: ["Qty", "Description", "Amount"],
        tableData: [],
    };

    fetchTimeDate = (timestamp) => {
        var iteim = new Date(timestamp).getDate();
        return iteim;
    };

    componentDidMount = async () => {
        let newArray = [];
        if (this.props.route.params.inHouse) {
            const responseJson = { data: this.props.route.params.orderInfo };

            for (let i = 0; i < responseJson.data.foodItems.length; i++) {
                let modifiedArray = [responseJson.data.foodItems[i].quantity, responseJson.data.foodItems[i].title,
                    "£ " + parseInt(responseJson.data.foodItems[i].price) * parseInt(
                        responseJson.data.foodItems[i].quantity)];
                newArray.push(modifiedArray);
            }

            let subTotal = 0;
            responseJson.data.foodItems.forEach(item => subTotal += (parseFloat(item.price) * parseInt(item.quantity)));

            this.setState(
                {
                    resturentName: responseJson.data.restaurant.name,
                    orderNumber: responseJson.data.orderNumber,
                    resturentLocation: responseJson.data.restaurant.address,
                    orderDate: responseJson.data.createdAt,
                    totalPrice: responseJson.data.totalPrice,
                    paymentMethod: responseJson.data.paymentMethod,
                    deliveryCharge: responseJson.data.deliveryCharges,
                    discount: 0,
                    discountCharge: 0,
                    name: responseJson.data.name,
                    userAddress: responseJson.data.address ? responseJson.data.address : "N/A",
                    userPhone: responseJson.data.phoneNumber,
                    tableData: newArray,
                    foodItems: responseJson.data.foodItems,
                    orderType: responseJson.data.orderType,
                    subTotal: subTotal,
                }
            );

            if (responseJson.data.orderType === 'collection'){
                this.setState({
                    data: [
                        { time: "0", title: "Received", lineColor: "#d2181b", circleColor: "#d2181b" },
                        { time: "0", title: "Collected", lineColor: "#adadad", circleColor: "#d2181b" }
                    ]
                });
            }
        } else {
            axios.get(`${this.props.uri}order/fetch/${this.props.route.params.orderId}`)
                .then(res => {
                    const responseJson = res.data;
                    for (let i = 0; i < responseJson.data.foodItems.length; i++) {
                        let modifiedArray = [responseJson.data.foodItems[i].quantity, responseJson.data.foodItems[i].title,
                            "£ " + parseInt(responseJson.data.foodItems[i].price) * parseInt(
                                responseJson.data.foodItems[i].quantity)];
                        newArray.push(modifiedArray);
                    }

                    let subTotal = 0;
                    responseJson.data.foodItems.forEach(item => subTotal += (parseFloat(item.price) * parseInt(item.quantity)));

                    this.setState(
                        {
                            resturentName: responseJson.data.restaurant.name,
                            orderNumber: responseJson.data.orderNumber,
                            resturentLocation: responseJson.data.restaurant.address,
                            orderDate: responseJson.data.createdAt,
                            totalPrice: responseJson.data.totalPrice,
                            paymentMethod: responseJson.data.paymentMethod,
                            deliveryCharge: responseJson.data.deliveryCharge,
                            discount: responseJson.data.discount,
                            discountCharge: 0.50,
                            name: responseJson.data.user.name,
                            userAddress: responseJson.data.user.address + "," + responseJson.data.user.postCode,
                            userPhone: responseJson.data.user.mobile,
                            tableData: newArray,
                            foodItems: responseJson.data.foodItems,
                            orderType: responseJson.data.orderType,
                            subTotal: subTotal,
                        },
                    );
                });
        }
    };

    getPrintContentGapText = (size) => {
        let i;
        let gap = "";
        for (i = 0; i < size; i++) {
            gap += " ";
        }
        return gap;
    };

    getPrintContentGap = (item) => {
        const { title, quantity, price } = item;
        const leftContentLength = title.length + (quantity.toString()).length + 2; // Here 2 for parenthesis
        const rightContentLength = (price.toString()).length === 1 ? ((price.toString()).length + 3) :
            ((parseFloat(price).toFixed(2)).toString()).length;
        return this.getPrintContentGapText(32 - (leftContentLength + rightContentLength)); // Here 32 for page maximum character size
    };

    getSubtotalContentGap = (content, price) => {
        const leftContentLength = content.length;
        const rightContentLength = (price.toString()).length === 1 ? ((price.toString()).length + 3) :
            ((parseFloat(price).toFixed(2)).toString()).length;
        return this.getPrintContentGapText(32 - (leftContentLength + rightContentLength)); // Here 32 for page maximum character size
    };

    printBillTest = () => {
        /*var dates = new Date(this.state.orderDate);
         this.state.currentPrinter && BLEPrinter.printBill("<CM>"+this.state.resturentName+"</CM>\n<C>Address:"+this.state.resturentLocation+"</C>\n<C>Date:"+dates.getDate().toString()+"/"+dates.getMonth().toString()+"/"+dates.getFullYear().toString()+"</C>\n\n<C>Order no:"+this.state.orderNumber+"</C>\n<C>"+"QTY"+" "+" "+" "+" "+"Description"+" "+" "+" "+" "+"Amount</C>\n____________________________\n\n"+this.state.foodItems.map(arr=>"<C>"+arr.quantity.toString()+" "+" "+" "+" "+arr.title.toString()+" "+" "+" "+" "+" "+arr.price.toString()+"</C>\n")+"\n\n____________________________\n\n<C>GROSS TOTAL:"+" "+" "+" "+" "+" "+" "+this.state.totalPrice+"</C>\n<C>DISCOUNT CHARGES:"+" "+" "+" "+"" /+" "+" "+this.state.deliveryCharge+"  </C>\n<C>DELIVERY FEES:"+" "+" "+" "+" "+" "+this.state.deliveryCharge+"  </C>\n<C>SAVINGS</C>\n________________________________\n<L>  TOTAL PAYMENT (CASH/CARD)  </L><R>  "+this.state.totalPrice.toString()+"  </R>\n\n\n\n\n\n<C>DELIVERY TO: "+this.state.userName+", "+this.state.userAddress+"</C>\n\n<C>OR,</C>\n\n\n<C>COLLECTION FOR CUSTOMER NAME</C>");*/

        const {
            resturentName,
            resturentLocation,
            orderDate,
            orderNumber,
            foodItems,
            totalPrice,
            discount,
            deliveryCharge,
            orderType,
            name,
            userAddress,
            discountCharge,
        } = this.state;

        let subTotal = 0;
        foodItems.forEach(item => subTotal += (parseFloat(item.price) * parseInt(item.quantity)));

        const discountCharges = discountCharge;
        // const totalPaymentCash  = (subTotal - discount) + deliveryCharge + discountCharges;
        const seperator = "--------------------------------";

        /* Print Text PrinterHeader */
        const restaurantNameText = `<CM>${resturentName || "N/A"}</CM>\n`;
        const restaurantLocationText = `<C>${resturentLocation || "N/A"}</C>\n`;
        const orderDateAndOrderNoText = `<C>${orderDate ? moment(orderDate).format("DD/MM/YYYY") :
            "N/A"}, Order No: ${orderNumber || "N/A"}</C>\n\n`;

        /* Description Body */
        const descriptionHeader = `Description(QTY)     Amount(GBP)\n${seperator}\n`;

        let descriptionBody = "";
        foodItems.forEach(item => {
            descriptionBody += `${item.title}(${item.quantity})${this.getPrintContentGap(item)}${parseFloat(item.price).toFixed(
                2)}\n`;
        });

        /* Sub Total Body */
        const subtotalText = `Sub Total${this.getSubtotalContentGap("Sub Total", subTotal)}${parseFloat(subTotal).toFixed(
            2)}\n`;
        const discountText = `Discount${this.getSubtotalContentGap("Discount", discount)}${parseFloat(discount).toFixed(
            2)}\n`;
        const deliveryChargeText = `Delivery Charges${this.getSubtotalContentGap("Delivery Charges", deliveryCharge)}${parseFloat(
            deliveryCharge).toFixed(2)}\n`;
        const discountChargeText = `Discount Charges${this.getSubtotalContentGap("Discount Charges",
            discountCharges)}${parseFloat(discountCharges)
            .toFixed(2)}\n`;
        const totalPaymentText = `Total Payment (CASH)${this.getSubtotalContentGap("Total Payment (CASH)",
            totalPrice)}${parseFloat(totalPrice).toFixed(
            2)}\n\n`;

        /* Address */
        const deliveryTo = `<D>Delivery: ${userAddress || "N/A"}</D>`;
        const collectionFor = `<D>Collection: ${name || "N/A"}</D>`;

        const printTextHeader = `${restaurantNameText}${restaurantLocationText}${orderDateAndOrderNoText}`;
        const printTextDescriptionBody = `${descriptionHeader}${descriptionBody}${seperator}\n`;
        const subtotalBody = `${subtotalText}${discountText}${deliveryChargeText}${discountChargeText}${seperator}\n${totalPaymentText}`;
        const printAddress = ["delivery", "home-delivery"].includes(orderType) ? deliveryTo : collectionFor;
        const printText = `${printTextHeader}${printTextDescriptionBody}${subtotalBody}${printAddress}`;

        this.state.currentPrinter && BLEPrinter.printBill(printText);
    };

    _connectPrinter = (printer) => {
        BLEPrinter.connectPrinter(printer).then(value => {
            this.setState({ currentPrinter: value });
        })
            .then(() => {
                this.printBillTest();
            }).then(() => {
            this.setState({ printing: false });
            ToastAndroid.show("Printing completed", ToastAndroid.CENTER, ToastAndroid.SHORT);
        });
    };

    printContent = async () => {
        await BluetoothStateManager.enable().then((result) => {
            BLEPrinter.init().then(() => {
                BLEPrinter.getDeviceList().then(value => {
                    if (this.props.activeBluetoothPrinter === "") {
                        ToastAndroid.show("Set your printer first", ToastAndroid.CENTER, ToastAndroid.SHORT);
                    } else {
                        this._connectPrinter(this.props.activeBluetoothPrinter);
                    }

                });
            });
        });

    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#fff" }}>
                <Header style={{ backgroundColor: "white" }}
                        androidStatusBarColor="white">
                    <Left>
                        <Button onPress={() => {
                            this.props.navigation.goBack();
                        }} transparent>
                            <Icon style={{ color: "black" }} name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{ color: "black" }}>Order Details</Title>
                    </Body>
                    <Right>
                        <Button onPress={() => {
                            this.printContent();
                        }}
                                style={{ backgroundColor: "#d2181b", height: 30, paddingLeft: 15, paddingRight: 15 }}>
                            <Text style={{ fontSize: 10, color: "white" }}>Reprint</Text>
                        </Button>
                    </Right>
                </Header>
                <ScrollView style={{ flex: 1, marginBottom: 20 }}>
                    {/*<Timeline
                        data={this.state.data}
                        style={{ marginLeft: 70, marginRight: 70, marginTop: 50 }}
                        columnFormat="single-column-right"
                    />*/}
                    {/*<View style={{ width: "100%", height: 1, backgroundColor: "#000", marginTop: 20 }} />*/}
                    <View style={{ width: "100%", paddingTop: 10 }}>
                        <Text style={{ fontSize: 14, fontWeight: "bold", alignSelf: "center" }}>{this.state.resturentName}</Text>
                        <Text style={{ fontSize: 14, alignSelf: "center" }}>{this.state.resturentLocation}</Text>
                        <Text style={{ fontSize: 14, alignSelf: "center" }}>{moment(this.state.orderDate).format(
                            "DD/MM/YYYY")}</Text>
                        <Text style={{ fontSize: 14, alignSelf: "center" }}>{this.state.orderNumber}</Text>
                    </View>
                    <Table borderStyle={{ borderWidth: 2, borderColor: "#fff" }}>
                        <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text} />
                        <Rows data={this.state.tableData} textStyle={styles.text} />
                    </Table>
                    <View style={{ width: "100%", height: 1, backgroundColor: "#000", marginTop: 20 }} />
                    <View style={{
                        flexDirection: "row", justifyContent: "space-between", marginRight: 20, marginLeft: 20, marginTop: 20,
                    }}>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>Sub Total:</Text>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>£{parseFloat(this.state.subTotal).toFixed(2)}</Text>
                    </View>
                    <View style={{
                        flexDirection: "row", justifyContent: "space-between", marginRight: 20, marginLeft: 20, marginTop: 20,
                    }}>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>Discount:</Text>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>£{parseFloat(this.state.discount).toFixed(2)}</Text>
                    </View>
                    <View style={{
                        flexDirection: "row", justifyContent: "space-between", marginRight: 20, marginLeft: 20, marginTop: 20,
                    }}>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>Discount Charges:</Text>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>
                            £{this.state.discountCharge ? parseFloat(this.state.discountCharge).toFixed(2) : "0.00"}
                        </Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginRight: 20, marginLeft: 20, marginTop: 20 }}>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>Delivery Fee:</Text>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>
                            £ {this.state.deliveryCharge ? parseFloat(this.state.deliveryCharge).toFixed(2) : "0.00"}
                        </Text>
                    </View>
                    <View style={{
                        flexDirection: "row", justifyContent: "space-between", marginRight: 20, marginLeft: 20, marginTop: 20,
                        backgroundColor: "#e4e4e4", padding: 10,
                    }}>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>
                            Total Payment ({this.state.paymentMethod === "cash" ? "Cash" : this.state.paymentMethod}):
                        </Text>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>£{this.state.totalPrice}</Text>
                    </View>
                    <View style={{ alignItems: "center", marginRight: 20, marginLeft: 20, marginTop: 20 }}>
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>
                            {this.state.orderType !== "collection" ? 'Delivered To:' : 'Collection For:'} {this.state.name}
                        </Text>
                        {this.state.orderType !== "collection" &&
                        <Text style={{ fontWeight: "bold", fontSize: 14 }}>{this.state.userAddress}</Text>
                        }
                        <Icon onPress={() => {
                            Linking.openURL(`tel:${this.state.userPhone}`);
                        }} type="Entypo" name="phone" style={{ fontSize: 38, color: "green", marginTop: 50 }} />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
        },
        list: {
            marginTop: 20,
            marginLeft: 60,
            marginRight: 50,
            marginBottom: -500,
        },
        head: {
            height: 40,
            backgroundColor: "#fff",
        },
        text: {
            margin: 6,
            textAlign: "left",
            paddingLeft: 20,
            fontWeight: "bold",
        },

    },
);

const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        uri: state.auth.uri,
        activeBluetoothPrinter: state.auth.activeBluetoothPrinter,
        printerType: state.auth.printer,
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Order);
