import React from "react";
import {View, Text, SafeAreaView, StyleSheet, ScrollView, TouchableOpacity} from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import globalStyles from "../../assets/styles/globalStyles";
import Ionicons from "react-native-vector-icons/Ionicons";
import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {showToastWithGravityAndOffset} from "../../shared-components/ToastMessage";
import {useDispatch} from "react-redux";

const Settings = ({navigation}) => {
    const dispatch = useDispatch();

    const logOut = async () => {
        await AsyncStorage.clear();
        dispatch({type: "LOGOUT", logged: false});
        showToastWithGravityAndOffset("Logged out successfully!");
    };

    return (
        <SafeAreaView style={styles.container}>
            <View style={[styles.headerTop, globalStyles.boxShadow]}>
                <Text style={[globalStyles.headerText, globalStyles.fw600, globalStyles.f22]}>
                    Settings
                </Text>
            </View>
            <ScrollView>
                <View style={[globalStyles.paddingHorizontal5, globalStyles.paddingVertical5]}>
                    {/*<TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("BusinessProfile")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <FontAwesome5 name="user-tie" size={22} color="#424242" />
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Business Profile
                        </Text>
                    </TouchableOpacity>*/}
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("BusinessProfile")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <FontAwesome5 name="user-tie" size={22} color="#424242"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Business Profile
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("MyOrder")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <FontAwesome5 name="baby-carriage" size={22} color="#424242"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            My Orders
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("InHouseOrder")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <FontAwesome5 name="house-user" size={22} color="#424242"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            In-House
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("MyBookings")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <MaterialCommunityIcons name="storefront-outline" size={24} color="black"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            My Bookings
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("SalesReport")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <FontAwesome name="line-chart" size={24} color="black"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Sales Report
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("Invoice")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <FontAwesome5 name="file-invoice" size={24} color="black" />
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Invoice
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("Notifications")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <Ionicons name="notifications-outline" size={22} color="#424242"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Notifications
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("TermsAndConditions")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <Ionicons name="document-text-outline" size={22} color="#424242"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Terms & Conditions
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => navigation.navigate("Printer")}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <AntDesign name="printer" size={24} color="black"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Printer
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[globalStyles.flexDirectionRow, globalStyles.paddingBottom3]}
                        onPress={() => logOut()}
                    >
                        <View style={[styles.circleIconArea, globalStyles.boxShadow, globalStyles.elevation3]}>
                            <AntDesign name="logout" size={22} color="#424242"/>
                        </View>
                        <Text
                            style={[globalStyles.f18, globalStyles.paddingTop1, globalStyles.paddingLeft5]}>
                            Sign Out
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
        },
        headerTop: {
            flexDirection: "row",
            justifyContent: "space-between",
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("2%"),
        },
        circleIconArea: {
            backgroundColor: "#fff",
            width: 40,
            height: 40,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 20,
        },
    },
);

export default Settings;
