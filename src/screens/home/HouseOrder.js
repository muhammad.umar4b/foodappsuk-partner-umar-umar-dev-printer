import React, { Component } from 'react'
import { View, Text,StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import AddNewDelivery from "./AddNewDelivery"
import DeliveryList from "./DeliveryList"
import {Container,Header,Tabs,Tab} from "native-base"

export class HouseOrder extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Tabs tabBarUnderlineStyle={{backgroundColor:'#d2181b'}}>
                  <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}} tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white", }} heading="Add New Delivery">
                      <AddNewDelivery navigation={this.props.navigation} />
                  </Tab>
                  <Tab textStyle={{color:'black'}} activeTextStyle={{color:'#d2181b'}}  tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white" }} heading="Delivery List">
                      <DeliveryList navigation={this.props.navigation} />
                  </Tab>
                </Tabs>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff'
    }
})

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(HouseOrder)
