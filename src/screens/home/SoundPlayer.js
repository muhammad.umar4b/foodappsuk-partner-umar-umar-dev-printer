import React from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import SoundPlayer from "react-native-sound-player";

const SoundPlayerComponent = () => {
    const onPress = () => {
        alert('Starting')
        try {
            // SoundPlayer.playSoundFile("nuclearalarm", "mp3");
            SoundPlayer.playUrl('https://raw.githubusercontent.com/zmxv/react-native-sound-demo/master/advertising.mp3');
        } catch (e) {
            console.log(`cannot play the sound file`, e);
        }
    };

    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity style={{backgroundColor: 'red', padding: 15}} onPress={onPress}>
                <Text>Press</Text>
            </TouchableOpacity>
        </View>
    );
};

export default SoundPlayerComponent;
