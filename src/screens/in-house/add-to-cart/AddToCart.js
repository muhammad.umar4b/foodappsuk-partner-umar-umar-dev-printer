import React, { useEffect, useLayoutEffect, useState } from "react";
import { ScrollView } from "react-native";

import { useDispatch, useSelector } from "react-redux";

import styles from "./styles";

import { SET_CART_LIST } from "../../../store/types";

import CartItemArea from "./CartItemArea";
import FormArea from "./FormArea";

const AddToCart = ({ navigation }) => {
    const globalState = useSelector(state => state);
    const dispatch = useDispatch();
    const [state, setState] = useState({
        subTotal: "",
    });

    const {
        restaurant: {
            cartList,
            restaurantData,
        },
    } = globalState;

    useEffect(() => {
        if (cartList) {
            const {
                note,
                subTotal,
            } = cartList;

            setState({ note, subTotal });
        } else setState({ note: "", subTotal: 0 });
    }, [cartList]);

    useLayoutEffect(() => {
        navigation.setOptions({
            title: restaurantData ? restaurantData.name : "Restaurant",
        });
    }, [navigation, restaurantData]);

    const addToCart = async index => {
        let updateCartList = { ...cartList };

        updateCartList.foodItems = updateCartList.foodItems.map((item, key) => {
            if (key === index) {
                item.quantity = item.quantity + 1;
                item.price = parseFloat(parseFloat((item.basePrice * item.quantity) + (item.optionsPrice * item.quantity)).toFixed(2));
                updateCartList.subTotal = parseFloat(parseFloat(updateCartList.subTotal + item.basePrice + item.optionsPrice).toFixed(2));
            }

            return item;
        });

        dispatch({ type: SET_CART_LIST, payload: updateCartList });
    };

    const removeFromCart = async index => {
        let updateCartList = { ...cartList };

        const cartItem = updateCartList.foodItems.find((item, key) => index === key);

        if (cartItem.quantity === 1) {
            updateCartList.foodItems = updateCartList.foodItems.filter((item, key) => key !== index);

            let subTotal = 0;
            updateCartList.foodItems.forEach(item => {
                subTotal = parseFloat(parseFloat(subTotal + item.price).toFixed(2));
            });

            updateCartList.subTotal = subTotal;
            updateCartList.count = updateCartList.foodItems.length;
        } else {
            updateCartList.foodItems = updateCartList.foodItems.map((item, key) => {
                if (key === index) {
                    item.quantity = item.quantity - 1;
                    item.price = parseFloat(parseFloat((item.basePrice * item.quantity) + (item.optionsPrice * item.quantity)).toFixed(2));
                    updateCartList.subTotal = parseFloat(parseFloat(updateCartList.subTotal - (item.basePrice + item.optionsPrice)).toFixed(2));
                }

                return item;
            });
        }

        dispatch({ type: SET_CART_LIST, payload: updateCartList.count === 0 ? null : updateCartList });
    };

    const {
        container,
    } = styles;

    return (
        <ScrollView contentContainerStyle={container} showsVerticalScrollIndicator={false}>
            <CartItemArea cartList={cartList} addToCart={addToCart} removeFromCart={removeFromCart} />
            <FormArea state={state} dispatch={dispatch} navigation={navigation} cartList={cartList} />
        </ScrollView>
    );
};

export default AddToCart;
