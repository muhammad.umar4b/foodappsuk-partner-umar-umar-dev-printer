import React, {useEffect, useState} from 'react';
import {View, SafeAreaView, ScrollView} from 'react-native';

import axios from 'axios';
import {useSelector} from 'react-redux';

import globalStyles from '../../../assets/styles/globalStyles';

import {apiBaseUrl} from '../../../config/index-example';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

import RenderData from './RenderData';
import Loader from '../../../utilities/Loader';

const PendingOrder = ({navigation, setTotalPendingOrder, route}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const state = useSelector(state => state);
  const {
    restaurant: {
      restaurantData: {_id},
    },
  } = state;

  const getPendingOrders = async id => {
    try {
      const apiEndPoint =
        'order/get-restaurant-inhouse-pending-dine-in-orders/';
      const response = await axios.get(`${apiBaseUrl}${apiEndPoint}${id}`);

      if (response.data) {
        setData(response.data.data);
        setTotalPendingOrder(response.data.data.length);
      }

      setIsLoading(false);
      return true;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
        if (error.response.data.error)
          showToastWithGravityAndOffset(error.response.data.error);
      }
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getPendingOrders(_id).then(res => console.log('PENDING ORDERS: ', res));
  }, [_id, route]);

  const {flex1, paddingHorizontal5, paddingBottom3} = globalStyles;

  return isLoading ? (
    <Loader />
  ) : (
    <SafeAreaView style={flex1}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[paddingHorizontal5, paddingBottom3]}>
          <RenderData navigation={navigation} data={data} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default PendingOrder;
