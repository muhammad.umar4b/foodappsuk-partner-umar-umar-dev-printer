import React from "react";
import {Text} from "react-native";

import globalStyles from "../../../assets/styles/globalStyles";

import SingleItem from "./SingleItem";

const RenderData = (props) => {
    const {
        data,
        navigation,
    } = props;

    const {
        paddingTop1,
        bgWarning,
    } = globalStyles;

    const setStatusBgColor = (orderStatus) => {
        if (orderStatus === "Pending") return bgWarning;
    };

    return data.length > 0 ?
        data.map((item, index) => <SingleItem
            navigation={navigation}
            item={item}
            key={index}
            setStatusBgColor={setStatusBgColor}
        />)
        :
        <Text style={paddingTop1}>No Order Yet!</Text>;
};


export default RenderData;
