import React from "react";
import {ScrollView, Text, TouchableOpacity, View} from "react-native";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

export default function ServiceTypeArea(props) {
    const {
        serviceListArea,
        updateServiceType,
    } = props;

    const {
        restaurantInfoArea,
        serviceTypeArea,
        serviceArea,
        serviceAreaSingle,
        activeService,
    } = styles;

    const {
        marginLeft2,
        textDark,
        fw700,
        f18,
        textWhite,
    } = globalStyles;

    return (
        <View style={restaurantInfoArea}>
            <View style={serviceTypeArea}>
                <Text>Services Type</Text>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    style={serviceArea}
                    pagingEnabled={true}>
                    {serviceListArea.map((item, index) => (
                        <TouchableOpacity
                            onPress={() => updateServiceType(index)}
                            key={index}
                            style={[serviceAreaSingle, marginLeft2, item.isActive && activeService]}>
                            <Text
                                style={[textDark, fw700, f18, item.isActive && textWhite]}>
                                {item.name}
                            </Text>
                        </TouchableOpacity>
                    ))}
                </ScrollView>
            </View>
        </View>
    );
}
