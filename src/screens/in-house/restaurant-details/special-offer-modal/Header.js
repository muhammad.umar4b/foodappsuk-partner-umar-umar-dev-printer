import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import AntDesign from "react-native-vector-icons/AntDesign";

import styles from "../styles";

export default function Header(props) {
    const {
        specialOfferModalState,
        isSpecialOfferModalVisible,
        setIsSpecialOfferModalVisible,
        hideCategoryList,
    } = props;

    const {
        modalHeaderArea,
        modalHeaderField,
        modalCloseIconArea,
    } = styles;

    const {
        name,
    } = specialOfferModalState;

    return <View style={modalHeaderArea}>
        <Text style={modalHeaderField}>
            {name}
        </Text>
        <TouchableOpacity
            style={modalCloseIconArea}
            onPress={() => {
                setIsSpecialOfferModalVisible(!isSpecialOfferModalVisible);
                hideCategoryList();
            }}
        >
            <AntDesign name="close" size={18} color="#fff" />
        </TouchableOpacity>
    </View>;
}
