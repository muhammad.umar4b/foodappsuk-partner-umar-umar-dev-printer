import React from "react";
import { ScrollView, Text, View, Pressable } from "react-native";

import AntDesign from "react-native-vector-icons/AntDesign";

import FoodItemArea from "./FoodItemArea";
import Loader from "../../../utilities/Loader";

import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

export default function CategoryArea(props) {
    const {
        categoryList,
        toggleCategory,
        toggleAddToCart,
        isViewBasket,
        activeCategory,
        foodItemList,
        foodItemLoading,
        categoryListLoading,
    } = props;

    const {
        categoryArea,
        categoryAreaSingle,
        categoryView,
        categoryViewActive,
        categoryHeaderText,
    } = styles;

    const {
        boxShadow,
        textWhite,
        textDark,
        f20,
        fw700,
        paddingTop5,
    } = globalStyles;

    const customHeight = isViewBasket ? { height: hp("65%") } : { height: hp("70%") };

    return (
        <View style={[categoryArea, customHeight]}>
            <Text style={[categoryHeaderText]}>Menu</Text>
            <ScrollView showsVerticalScrollIndicator={false}>
                {categoryList.map((item, index) => (
                    <View style={[categoryAreaSingle, boxShadow]} key={index}>
                        <Pressable
                            style={[categoryView, item.isActive ? categoryViewActive : {}]}
                            onPress={() => toggleCategory(item._id)}
                        >
                            <Text style={[f20, fw700, item.isActive ? textWhite : textDark]}>
                                {item.name}
                            </Text>
                            <AntDesign
                                name={item.isActive ? "downcircleo" : "upcircleo"}
                                size={22}
                                color={item.isActive ? "#fff" : "#000"}
                            />
                        </Pressable>

                        {item.isActive &&
                            <FoodItemArea
                                foodItemLoading={foodItemLoading}
                                activeCategory={activeCategory}
                                foodItemList={foodItemList}
                                toggleAddToCart={toggleAddToCart}
                            />
                        }
                    </View>
                ))}
                {categoryListLoading &&
                    <View style={paddingTop5}>
                        <Loader />
                    </View>
                }

            </ScrollView>
        </View>
    );
}
