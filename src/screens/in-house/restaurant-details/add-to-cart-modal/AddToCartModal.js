import React from "react";
import {ScrollView, Text, View} from "react-native";

import Modal from "react-native-modal";

import globalStyles from "../../../../assets/styles/globalStyles";

import Body from './Body';
import Header from './Header';

export default function AddToCartModal(props) {
    const {
        state,
        setState,
        modalState,
        isModalVisible,
        setModalVisible,
        setIsViewBasket,
        hideCategoryList
    } = props;

    const {
        modalView,
    } = globalStyles;

    return <Modal
        isVisible={isModalVisible}
        animationIn="fadeIn"
        animationOut="fadeOut"
        animationInTiming={500}
        animationOutTiming={500}
        style={modalView}>
        {modalState ?
            <View>
                <ScrollView>
                    <Header
                        modalState={modalState}
                        isModalVisible={isModalVisible}
                        setModalVisible={setModalVisible}
                        hideCategoryList={hideCategoryList}
                    />
                    <Body
                        state={state}
                        setState={setState}
                        modalState={modalState}
                        isModalVisible={isModalVisible}
                        setModalVisible={setModalVisible}
                        setIsViewBasket={setIsViewBasket}
                        hideCategoryList={hideCategoryList}
                    />
                </ScrollView>
            </View> :
            <View>
                <Text>Not found!</Text>
            </View>
        }
    </Modal>;
}
