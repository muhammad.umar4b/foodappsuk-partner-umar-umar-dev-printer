import React from "react";
import { Text, TextInput, View } from "react-native";

import Feather from "react-native-vector-icons/Feather";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

const PhoneNumberInput = (props) => {
    const {
        state,
        setState,
        phoneNumber,
    } = props;

    const {
        card,
        boxShadow,
        marginTop3,
        cardHeader,
        elevation5,
        marginLeft06,
        paddingTop3,
        flexDirectionRow,
        paddingHorizontal5,
    } = globalStyles;

    const {
        inputField,
        circleIconArea,
        cardHeaderLabel,
    } = styles;

    return (
        <View style={paddingHorizontal5}>
            <View style={[card, boxShadow, marginTop3]}>
                <View style={cardHeader}>
                    <View style={flexDirectionRow}>
                        <View style={[circleIconArea, boxShadow, elevation5, marginLeft06]}>
                            <Feather name="smartphone" size={20} color="#D2181B" />
                        </View>
                        <Text style={cardHeaderLabel}>Guest Number</Text>
                    </View>
                </View>
                <View style={paddingTop3}>
                    <TextInput
                        value={phoneNumber}
                        onChangeText={value => setState({ ...state, phoneNumber: value })}
                        editable={true}
                        style={inputField}
                        keyboardType={"default"}
                        placeholder="Guest Number (optional)"
                    />
                </View>
            </View>
        </View>
    );
};


export default PhoneNumberInput;
