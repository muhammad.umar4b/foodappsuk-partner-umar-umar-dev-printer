import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

import ButtonLoader from "../../../utilities/ButtonLoader";

const ButtonInput = (props) => {
    const {
        placeOrder,
        isLoading,
        service_type,
        savedDineIn,
    } = props;

    const {
        flexDirectionRow,
        paddingHorizontal5,
        marginLeft8,
        f20,
    } = globalStyles;

    const {
        continueButtonArea,
        continueButton,
        continueText,
    } = styles;

    return (
        <View style={paddingHorizontal5}>
            <View style={[continueButtonArea, flexDirectionRow]}>
                <TouchableOpacity
                    style={continueButton}
                    onPress={() => {
                        placeOrder("save").then(res => console.log("ORDER RESPONSE: ", res));
                    }}
                >
                    <Text style={[continueText, f20]}>
                        {isLoading ? <ButtonLoader /> : (service_type === "dine_in" ? "Save" : "Submit")}
                    </Text>
                </TouchableOpacity>

                {(service_type === "dine_in" && savedDineIn) &&
                    <TouchableOpacity
                        style={[continueButton, marginLeft8]}
                        onPress={() => {
                            placeOrder("serve").then(res => console.log("ORDER RESPONSE: ", res));
                        }}
                    >
                        <Text style={[continueText, f20]}>
                            Serve
                        </Text>
                    </TouchableOpacity>
                }
            </View>
        </View>
    );
};

export default ButtonInput;
