import React from "react";
import { Text, TextInput, View } from "react-native";

import Feather from "react-native-vector-icons/Feather";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

const CustomerNameInput = (props) => {
    const {
        state,
        setState,
        customerName,
    } = props;

    const {
        card,
        boxShadow,
        marginTop3,
        cardHeader,
        elevation5,
        marginLeft06,
        paddingTop3,
        flexDirectionRow,
        paddingHorizontal5,
    } = globalStyles;

    const {
        inputField,
        circleIconArea,
        cardHeaderLabel,
    } = styles;

    return (
        <View style={paddingHorizontal5}>
            <View style={[card, boxShadow, marginTop3]}>
                <View style={cardHeader}>
                    <View style={flexDirectionRow}>
                        <View
                            style={[circleIconArea, boxShadow, elevation5, marginLeft06]}>
                            <Feather name="user" size={20} color="#D2181B" />
                        </View>
                        <Text style={cardHeaderLabel}>Customer Name</Text>
                    </View>
                </View>
                <View style={paddingTop3}>
                    <TextInput
                        value={customerName}
                        onChangeText={value => setState({ ...state, customerName: value })}
                        editable={true}
                        style={inputField}
                        keyboardType={"default"}
                        placeholder="Name (optional)"
                    />
                </View>
            </View>
        </View>
    );
};


export default CustomerNameInput;
