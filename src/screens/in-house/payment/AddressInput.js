import React, { useState } from "react";
import { Text, TextInput, TouchableOpacity, View } from "react-native";

import Ionicons from "react-native-vector-icons/Ionicons";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

import { calculateDeliveryCharge } from "../../../store/actions/order";
import { showToastWithGravityAndOffset } from "../../../shared-components/ToastMessage";
import ButtonLoader from "../../../utilities/ButtonLoader";

const AddressInput = (props) => {
    const [isLoading, setIsLoading] = useState(false);

    const {
        state,
        setState,
        address,
        postCode,
        restaurantData,
        deliveryAddressInputEditable,
        setDeliveryAddressInputEditable,
    } = props;

    const addDeliveryAddress = () => {
        if (deliveryAddressInputEditable) {
            if (!address) {
                showToastWithGravityAndOffset("Address field is required!");
                return false;
            }

            if (!postCode) {
                showToastWithGravityAndOffset("Post Code field is required!");
                return false;
            }

            setIsLoading(true);

            const payload = {
                postCode,
                restaurantPostCode: restaurantData.postCode,
                restaurant: restaurantData._id,
            };

            calculateDeliveryCharge(payload).then(res => {
                console.log("DELIVERY CHARGE: ", !!res);
                console.log(res, "res");
                if (res) {
                    setState({ ...state, deliveryCharge: res });
                    setIsLoading(false);
                }
            });
        }

        setDeliveryAddressInputEditable(!deliveryAddressInputEditable);
    };

    const {
        card,
        boxShadow,
        marginTop3,
        cardHeader,
        paddingTop3,
        flexDirectionRow,
        paddingHorizontal5,
        paddingTop02,
        paddingLeft05,
        paddingTop2,
        paddingVertical2,
    } = globalStyles;

    const {
        inputField,
        cardHeaderLabel,
        continueButton,
        continueText,
    } = styles;

    return (
        <View style={paddingHorizontal5}>
            <View style={[card, boxShadow, marginTop3]}>
                <View style={cardHeader}>
                    <View style={flexDirectionRow}>
                        <Ionicons style={paddingTop02} name="md-location-outline" size={26} color="#D2181B" />
                        <Text style={[cardHeaderLabel, paddingLeft05]}>Delivery Address</Text>
                    </View>
                    <TouchableOpacity style={[continueButton, paddingVertical2, paddingHorizontal5]}
                                      onPress={() => addDeliveryAddress()}>
                        <Text style={continueText}>
                            {isLoading ? <ButtonLoader /> : (deliveryAddressInputEditable ? "Save" : "Edit")}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={paddingTop3}>
                    <TextInput
                        value={address}
                        onChangeText={value => setState({ ...state, address: value })}
                        editable={deliveryAddressInputEditable}
                        style={inputField}
                        keyboardType={"default"}
                        placeholder={"Address"}
                        multiline={true}
                        numberOfLines={4}
                    />
                </View>
                <View style={paddingTop2}>
                    <TextInput
                        value={postCode}
                        onChangeText={value => setState({ ...state, postCode: value })}
                        editable={deliveryAddressInputEditable}
                        style={inputField}
                        keyboardType={"default"}
                        placeholder={"Post Code"}
                    />
                </View>
            </View>
        </View>
    );
};


export default AddressInput;
