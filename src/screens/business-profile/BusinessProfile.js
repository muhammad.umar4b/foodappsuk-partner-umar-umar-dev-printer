import React, { useState } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";

import styles from "./styles";

// Component
import OwnerProfile from "./owner/OwnerProfile";
import RestaurantProfile from "./restaurant-profile/RestaurantProfile";

const ownerTab = 0;
const RestaurantProfileTab = 1;

const BusinessProfile = () => {
    const [activeTab, setActiveTab] = useState(ownerTab);

    const {
        container,
        tabArea,
        tabButton,
        activeTabButton,
        activeTabButtonText,
        tabButtonText,
    } = styles;

    const activeTabStyle = index => activeTab === index ? [tabButton, activeTabButton] : [tabButton];
    const activeTabTextStyle = index => activeTab === index ? [tabButtonText, activeTabButtonText] : [tabButtonText];

    return (
        <ScrollView showsVerticalScrollIndicator={false} style={container}>
            <View style={tabArea}>
                <TouchableOpacity style={activeTabStyle(ownerTab)} onPress={() => setActiveTab(ownerTab)}>
                    <Text style={activeTabTextStyle(ownerTab)}>Owner</Text>
                </TouchableOpacity>
                <TouchableOpacity style={activeTabStyle(RestaurantProfileTab)}
                                  onPress={() => setActiveTab(RestaurantProfileTab)}>
                    <Text style={activeTabTextStyle(RestaurantProfileTab)}>Business</Text>
                </TouchableOpacity>
            </View>

            {activeTab === ownerTab && <OwnerProfile />}
            {activeTab === RestaurantProfileTab && <RestaurantProfile />}
        </ScrollView>
    );
};

export default BusinessProfile;
