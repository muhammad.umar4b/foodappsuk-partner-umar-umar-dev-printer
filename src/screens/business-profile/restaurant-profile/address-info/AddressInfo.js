import React from "react";
import { View } from "react-native";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

import Body from "./Body";
import Header from "./Header";

const AddressInfo = (props) => {
    const {
        state,
        setState,
        addressInputEditable,
        setAddressInputEditable,
        updateRestaurantAddressInfo,
    } = props;

    const {
        profileArea,
    } = styles;

    const {
        card,
        boxShadow,
    } = globalStyles;

    const {
        address,
        postCode,
        phoneNo,
    } = state;

    const updateRestaurantAddress = () => {
        if (addressInputEditable) {
            const payload = {
                address,
                postCode,
                phoneNo,
            };

            updateRestaurantAddressInfo(payload);
        } else setAddressInputEditable(!addressInputEditable);
    };

    return (
        <View style={[card, boxShadow, profileArea]}>
            <Header
                state={state}
                updateRestaurantAddress={updateRestaurantAddress}
                inputEditable={addressInputEditable}
            />

            <Body
                state={state}
                setState={setState}
                inputEditable={addressInputEditable}
            />
        </View>
    );
};

export default AddressInfo;
