import React from "react";
import { Text, TextInput, View } from "react-native";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

const Body = (props) => {
    const {
        state,
        setState,
        inputEditable,
    } = props;

    const {
        inputLabel,
        inputField,
    } = styles;

    const {
        paddingTop3,
        paddingBottom1,
    } = globalStyles;

    const {
        address,
        postCode,
        phoneNo,
    } = state;

    return (
        <>
            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>
                    Address
                </Text>
                <TextInput
                    value={address}
                    placeholder={"Address"}
                    editable={inputEditable}
                    style={inputField}
                    keyboardType={"default"}
                    onChangeText={text => setState({ ...state, address: text })}
                />
            </View>

            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>
                    Post Code
                </Text>
                <TextInput
                    value={postCode}
                    placeholder={"Post Code"}
                    editable={inputEditable}
                    style={inputField}
                    keyboardType={"default"}
                    onChangeText={text => setState({ ...state, postCode: text })}
                />
            </View>

            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>
                    Guest Number
                </Text>
                <TextInput
                    value={phoneNo}
                    placeholder={"Guest Number"}
                    editable={inputEditable}
                    style={inputField}
                    keyboardType={"default"}
                    onChangeText={text => setState({ ...state, phoneNo: text })}
                />
            </View>
        </>
    );
};

export default Body;
