import React from "react";
import { View } from "react-native";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

import Body from "./Body";
import Header from "./Header";

const RestaurantTimings = (props) => {
    const {
        state,
        setState,
        timingsInputEditable,
        setTimingsInputEditable,
        updateRestaurantTimingsInfo,
    } = props;

    const {
        profileArea,
    } = styles;

    const {
        card,
        boxShadow,
    } = globalStyles;

    const {
        address,
        postCode,
        phoneNo,
    } = state;

    console.log(timingsInputEditable, 'timingsInputEditable');

    const updateRestaurantTimings = () => {
        /*if (timingsInputEditable) {
            const payload = {
                address,
                postCode,
                phoneNo,
            };

            updateRestaurantTimingsInfo(payload);
        } else setTimingsInputEditable(!timingsInputEditable);*/

        setTimingsInputEditable(!timingsInputEditable);
    };

    return (
        <View style={[card, boxShadow, profileArea, globalStyles.marginBottom5]}>
            <Header
                updateRestaurantTimings={updateRestaurantTimings}
                inputEditable={timingsInputEditable}
            />

            <Body
                state={state}
                setState={setState}
                inputEditable={timingsInputEditable}
            />
        </View>
    );
};


export default RestaurantTimings;
