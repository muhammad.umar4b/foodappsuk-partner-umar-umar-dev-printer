import React, { useEffect, useState } from "react";
import { View, SafeAreaView, ScrollView } from "react-native";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

import { getOwnerInfo, updateOwner } from "../../../store/actions/owner";
import { showToastWithGravityAndOffset } from "../../../shared-components/ToastMessage";

// Component
import Body from "./Body";
import Header from "./Header";
import Loader from "../../../utilities/Loader";

const OwnerProfile = () => {
    const [inputEditable, setInputEditable] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [state, setState] = useState({
        ownerName: "",
        email: "",
        mobileNo: "",
    });

    const getOwner = () => {
        getOwnerInfo().then(res => {
            console.log("OWNER INFO: ", !!res);

            const {
                email,
                mobileNo,
                ownerName,
            } = res;

            const updateState = { ...state };

            updateState.email = email;
            updateState.ownerName = ownerName;
            updateState.mobileNo = mobileNo;

            setState(updateState);
            setIsLoading(false);
        });
    };

    useEffect(() => {
        getOwner();
    }, []);

    const updateUser = async () => {
        if (inputEditable) {
            updateOwner(state).then(res => {
                console.log("UPDATE OWNER: ", !!res);

                showToastWithGravityAndOffset("Owner updated successfully!");
                setInputEditable(false);
                getOwner();
            });
        } else setInputEditable(true);
    };

    const {
        profileArea,
    } = styles;

    const {
        card,
        boxShadow,
        marginTop5,
    } = globalStyles;

    return isLoading ? <Loader style={marginTop5} /> :
        <SafeAreaView>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={[card, boxShadow, profileArea]}>
                    <Header updateUser={updateUser} inputEditable={inputEditable} />
                    <Body state={state} setState={setState} inputEditable={inputEditable} />
                </View>
            </ScrollView>
        </SafeAreaView>;
};

export default OwnerProfile;
