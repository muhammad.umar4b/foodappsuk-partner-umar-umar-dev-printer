import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    statusButtonText: {
        paddingHorizontal: wp("1.5%"),
        textTransform: "uppercase",
        fontWeight: "700",
        color: "#fff",
    },
    totalAmountArea: {
        borderTopColor: "#b4b4b4",
        borderTopWidth: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: hp("1%"),
    },
    circleIconArea: {
        backgroundColor: "#fff",
        width: 30,
        height: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15,
    },
    pendingButtonArea: {
        paddingTop: hp("1.5%"),
        flexDirection: "row",
        justifyContent: "center",
    },
    acceptButton: {
        backgroundColor: "#635d5d",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("1%"),
        marginRight: wp("2%"),
        borderRadius: 8,
    },
    declineButton: {
        backgroundColor: "#d2181b",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("1%"),
        borderRadius: 8,
    },
});

export default styles;
