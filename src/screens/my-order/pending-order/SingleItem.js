import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import moment from "moment";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

const SingleItem = (props) => {
    const {
        navigation,
        item,
        index,
        setStatusBgColor,
        acceptOrder,
        declineOrder,
    } = props;

    const {
        _id,
        orderNumber,
        createdAt,
        orderStatus,
        paymentMethod,
        totalPrice,
        orderType,
        tableNo,
    } = item;

    const {
        statusButtonText,
        totalAmountArea,
        pendingButtonArea,
        acceptButton,
        declineButton,
    } = styles;

    const {
        card,
        boxShadow,
        marginTop2,
        flexDirectionRow,
        justifyBetween,
        textGrey,
        marginLeft2,
        paddingTop1,
        paddingBottom2,
        textCapitalize,
        textWhite,
    } = globalStyles;

    return (
        <View style={[card, boxShadow, marginTop2]}>
            <TouchableOpacity
                onPress={() => navigation.navigate("MyOrderDetails", { id: item._id })}
                key={index}
            >
                <View style={[flexDirectionRow, justifyBetween]}>
                    <View style={[flexDirectionRow]}>
                        <Text style={textGrey}>{orderNumber}</Text>
                        <View style={[marginLeft2, setStatusBgColor(orderStatus), { height: 22, borderRadius: 5 }]}>
                            <Text style={statusButtonText}>{orderStatus}</Text>
                        </View>
                    </View>
                </View>

                <View style={[flexDirectionRow, justifyBetween, paddingTop1]}>
                    <Text>Order Date</Text>
                    <Text>{moment(createdAt).format("DD MMM YYYY, hh:mm A") || "N/A"}</Text>
                </View>

                <View
                    style={[flexDirectionRow, justifyBetween, paddingTop1]}>
                    <Text>Payment Method</Text>
                    <Text style={textCapitalize}>{paymentMethod || "N/A"}</Text>
                </View>

                <View
                    style={[flexDirectionRow, justifyBetween, paddingTop1, orderType !== "Dine In" && paddingBottom2]}>
                    <Text>Order Type</Text>
                    <Text style={textCapitalize}>{orderType || "N/A"}</Text>
                </View>

                {orderType === "Dine In" &&
                    <View
                        style={[flexDirectionRow, justifyBetween, paddingTop1, paddingBottom2]}>
                        <Text>Table No</Text>
                        <Text style={textCapitalize}>{tableNo || "N/A"}</Text>
                    </View>
                }
            </TouchableOpacity>

            <View style={totalAmountArea}>
                <Text>Total Price</Text>
                <Text>£{parseFloat(totalPrice).toFixed(2)}</Text>
            </View>

            <View style={pendingButtonArea}>
                <TouchableOpacity style={acceptButton} onPress={() => acceptOrder(_id)}>
                    <Text style={textWhite}>Accept</Text>
                </TouchableOpacity>
                <TouchableOpacity style={declineButton} onPress={() => declineOrder(_id)}>
                    <Text style={textWhite}>Decline</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default SingleItem;
