import React, { useEffect, useLayoutEffect, useState } from "react";
import { View, Text, TouchableOpacity, BackHandler } from "react-native";

import { HeaderBackButton } from "@react-navigation/stack";
import { useFocusEffect } from "@react-navigation/native";

import styles from "../../assets/styles/allOrderStyles";

// Component
import AllOrder from "../../utilities/all-order/AllOrder";
import PendingOrder from "./pending-order/PendingOrder";

const allOrdersTab = 0;
const pendingOrdersTab = 1;

const MyOrder = ({ navigation, route }) => {
    const [activeTab, setActiveTab] = useState(allOrdersTab);
    const [totalOrder, setTotalOrder] = useState(0);
    const [pendingOrder, setPendingOrder] = useState(0);

    useEffect(() => {
        if (route.params && route.params.pending) {
            setActiveTab(pendingOrdersTab);
        } else setActiveTab(allOrdersTab);
    }, [route, navigation]);

    const onBackPress = () => {
        if (route.params && Object.keys(route.params).length > 0) {
            navigation.navigate("Settings");
        } else {
            navigation.goBack();
        }
        return true;
    };

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: (props) => (
                <HeaderBackButton
                    {...props}
                    onPress={onBackPress}
                />
            ),
        });
    }, [navigation]);

    useFocusEffect(
        React.useCallback(() => {
            BackHandler.addEventListener("hardwareBackPress", onBackPress);
            return () =>
                BackHandler.removeEventListener("hardwareBackPress", onBackPress);
        }, []),
    );

    const {
        container,
        tabArea,
        tabButton,
        activeTabButton,
        activeTabButtonText,
        tabButtonText,
    } = styles;

    const activeTabStyle = index => activeTab === index ? [tabButton, activeTabButton] : [tabButton];
    const activeTabTextStyle = index => activeTab === index ? [tabButtonText, activeTabButtonText] : [tabButtonText];

    return (
        <View style={container}>
            <View style={tabArea}>
                <TouchableOpacity style={activeTabStyle(0)} onPress={() => setActiveTab(0)}>
                    <Text style={activeTabTextStyle(0)}>
                        All Orders {totalOrder ? `(${totalOrder})` : ""}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={activeTabStyle(1)} onPress={() => setActiveTab(1)}>
                    <Text style={activeTabTextStyle(1)}>
                        Pending Orders {pendingOrder ? `(${pendingOrder})` : ""}
                    </Text>
                </TouchableOpacity>
            </View>

            {activeTab === allOrdersTab &&
                <AllOrder
                    navigation={navigation}
                    setTotalOrder={setTotalOrder}
                    route={route}
                />
            }

            {activeTab === pendingOrdersTab &&
                <PendingOrder
                    navigation={navigation}
                    setPendingOrder={setPendingOrder}
                    route={route}
                />
            }
        </View>
    );
};

export default MyOrder;
