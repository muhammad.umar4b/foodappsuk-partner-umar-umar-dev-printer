import React from "react";
import { View } from "react-native";

import globalStyles from "../../assets/styles/globalStyles";

const Divider = () => {
    const {
        borderBottomGrey,
        marginTop1,
        marginBottom1,
    } = globalStyles;

    const dividerStyle = [borderBottomGrey, marginTop1, marginBottom1];

    return (
        <View style={dividerStyle} />
    );
};


export default Divider;
