import React, {useState, useEffect} from 'react';
import {View, ScrollView} from 'react-native';

import axios from 'axios';
import {useSelector} from 'react-redux';
import {apiBaseUrl} from '../../config/index-example';

import Body from './Body';
import Header from './Header';
import Loader from '../../utilities/Loader';

import globalStyles from '../../assets/styles/globalStyles';

const Invoice = () => {
  const state = useSelector(state => state);
  const {
    restaurant: {restaurantData},
  } = state;

  const [invoiceData, setInvoiceData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const getInvoiceLast7Days = async restaurantId => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}order/get-restaurant-invoice-7-days/${restaurantId}`,
      );
      if (response.data) {
        setInvoiceData(response.data.data['invoice']);
        setIsLoading(false);
        return true;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  useEffect(() => {
    if (restaurantData) {
      getInvoiceLast7Days(restaurantData['_id']).then(res =>
        console.log('INVOICE ', res),
      );
    }
  }, [restaurantData]);

  const {paddingHorizontal5, paddingTop2} = globalStyles;

  return (
    <ScrollView>
      <View style={paddingHorizontal5}>
        {restaurantData ? (
          <Header restaurantData={restaurantData} />
        ) : (
          <Loader style={paddingTop2} />
        )}

        {isLoading ? (
          <Loader style={paddingTop2} />
        ) : (
          <Body invoiceData={invoiceData} />
        )}
      </View>
    </ScrollView>
  );
};

export default Invoice;
