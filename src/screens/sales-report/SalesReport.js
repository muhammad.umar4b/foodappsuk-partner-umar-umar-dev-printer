import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";

import { useSelector } from "react-redux";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

import Today from "./today/Today";
import TotalPrice from "./TotalPrice";
import LastSevenDays from "./last-seven-days/LastSevenDays";

const SalesReport = () => {
    const [activeTab, setActiveTab] = useState(0);
    const state = useSelector(state => state);
    const [totalReportToday, setTotalReportToday] = useState(0);
    const [totalReportLastSevenDays, setTotalReportLastSevenDays] = useState(0);
    const [totalPrice, setTotalPrice] = useState("0.00");

    const { restaurant: { restaurantData } } = state;

    const {
        container,
        tabArea,
        tabButton,
        activeTabButton,
        activeTabButtonText,
        tabButtonText,
    } = styles;

    const activeTabStyle = index => activeTab === index ? [tabButton, activeTabButton] : [tabButton];
    const activeTabTextStyle = index => activeTab === index ? [tabButtonText, activeTabButtonText] : [tabButtonText];

    return (
        <View style={container}>
            <View style={tabArea}>
                <TouchableOpacity style={activeTabStyle(0)} onPress={() => setActiveTab(0)}>
                    <Text style={activeTabTextStyle(0)}>
                        Today {totalReportToday ? `(${totalReportToday})` : ""}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity style={activeTabStyle(1)} onPress={() => setActiveTab(1)}>
                    <Text style={activeTabTextStyle(1)}>
                        Last 7 Days {totalReportLastSevenDays ? `(${totalReportLastSevenDays})` : ""}
                    </Text>
                </TouchableOpacity>
            </View>

            <TotalPrice totalPrice={totalPrice} />

            {activeTab === 0 && restaurantData &&
                <Today restaurantId={restaurantData._id}
                       setTotalReportToday={setTotalReportToday}
                       setTotalPrice={setTotalPrice}
                />}

            {activeTab === 1 && restaurantData &&
                <LastSevenDays restaurantId={restaurantData._id}
                               setTotalReportLastSevenDays={setTotalReportLastSevenDays}
                               setTotalPrice={setTotalPrice}
                />}
        </View>
    );
};

export default SalesReport;
