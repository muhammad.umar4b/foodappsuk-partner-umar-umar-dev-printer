import React from "react";
import { Text } from "react-native";

import globalStyles from "../../../assets/styles/globalStyles";

import SingleItem from "./SingleItem";

const RenderData = ({ data, acceptOrder, declineOrder }) => {
    return (
        data.length > 0 ?
            data.map((item, index) =>
                <SingleItem
                    key={index}
                    index={index}
                    item={item}
                    acceptOrder={acceptOrder}
                    declineOrder={declineOrder}
                />,
            )
            : <Text style={globalStyles.paddingLeft5}>No Booking Yet!</Text>
    );
};

export default RenderData;
