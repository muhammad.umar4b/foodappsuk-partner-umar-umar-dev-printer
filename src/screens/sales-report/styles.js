import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: "#e4e4e4",
        },
        tabArea: {
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: hp("1%"),
            marginHorizontal: wp("5%"),
        },
        tabButton: {
            paddingVertical: hp("2%"),
            backgroundColor: "#fff",
            width: wp("45%"),
        },
        tabButtonText: {
            textAlign: "center",
            color: "#000",
        },
        activeTabButton: {
            borderBottomColor: "#d2181b",
            borderBottomWidth: 2,
        },
        activeTabButtonText: {
            color: "#d2181b",
        },
        reportArea: {
            justifyContent: "space-between",
            marginBottom: hp("9%"),
        },
        totalPriceArea: {
            paddingHorizontal: wp("5%"),
            paddingTop: hp("1.5%"),
            borderTopColor: "#d4cfcf",
            borderTopWidth: 1,
        },
        notificationArea: {
            flexDirection: "row",
            justifyContent: "space-between",
            paddingVertical: hp("2%"),
            paddingHorizontal: wp("5%"),
            backgroundColor: "#fff",
            elevation: 5,
            borderRadius: 10,
            marginHorizontal: wp("5%"),
            marginBottom: hp("2%"),
        },
        notificationTime: {
            color: "#979494",
        },
        searchButtonArea: {
            borderRadius: 8,
            paddingHorizontal: wp("5%"),
            flexDirection: "row",
            marginTop: hp("1%"),
        },
        todayButton: {
            backgroundColor: "#fff",
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("1%"),
            borderTopLeftRadius: 8,
            borderBottomLeftRadius: 8,
        },
        sevenDaysButton: {
            backgroundColor: "#fff",
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("1%"),
            borderTopRightRadius: 8,
            borderBottomRightRadius: 8,
        },
        buttonText: {
            color: "#000",
            fontSize: wp("4%"),
            fontWeight: "bold",
        },
    },
);

export default styles;
